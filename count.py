#!/usr/bin/env python

import sys
import operator

f = sys.argv[1]
cnt = {}
lines = open(f).read().splitlines()
for i in range(1, len(lines)):
  l = lines[i]
  tmp = l.split(',')
  f = tmp[3]
  if f not in cnt:
    cnt[f] = 0
  cnt[f] += 1

sorted_cnt = sorted(cnt.items(), key=operator.itemgetter(1), reverse=True)
for k,v in sorted_cnt:
  print k,v
